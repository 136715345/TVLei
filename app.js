var express = require('express');
var path = require('path');
var fs = require('fs');
var app = express();
var session = require('express-session');
var cookieParser = require('cookie-parser');
var mongo = require('mongodb');
var ObjectID = require('mongodb').ObjectID;
var host = '127.0.0.1';
var port = '27017';
var server = new mongo.Server(host,port,{auto_reconnect:true});
var db = new mongo.Db('userDate',server,{safe:true});
var sport = process.env.PORT || 1337;

// var mongoose = require('mongoose')
// var mongoStore = require('connect-mongo')(express)
// var port = process.env.PORT || 3000

app.set('views', './app/views/pages')
app.set('view engine', 'jade')
app.use(express.static(__dirname));
app.use(cookieParser());
app.use(session({
    secret: '123456',
    name: 'lastSign',   //这里的name值得是cookie的name，默认cookie的name是：connect.sid
    cookie: {maxAge: 1000*60*60*24 },  //设置maxAge是24h，即24h后session和相应的cookie失效过期
    resave: false,
    saveUninitialized: true,
}));

var models_path = __dirname + '/app/models'
var walk = function(path) {
  fs
    .readdirSync(path)
    .forEach(function(file) {
      var newPath = path + '/' + file
      var stat = fs.statSync(newPath)

      if (stat.isFile()) {
        if (/(.*)\.(js|coffee)/.test(file)) {
          require(newPath);
        }
      }
      else if (stat.isDirectory()) {
        walk(newPath)
      }
    })
}
walk(models_path)

require('./config/router')(app)

app.listen(sport)
app.use(express.static(path.join(__dirname, 'public')))


