var Cart = require('../app/controllers/cart')
var User = require('../app/controllers/user')
var Product = require('../app/controllers/product')
var Order = require('../app/controllers/order')
var Manager = require('../app/controllers/manager')

module.exports = function(app) {


      //尚未添加每个接口更新 maxage 开始计时时间的功能，即只要刷新或请求接口，都会重新计时24h


      //-------后台
      app.get('/admin/:page?/:id?', function(req, res, next) {
        console.log(req.session.lastSign);
        if (req.session.lastSign) {
          //此处无作用
          var hour = 1000 * 60 * 60 * 24;
          req.session.cookie.expires = new Date(Date.now() + hour);
          req.session.cookie.maxAge = hour;

          if (!req.params.page || req.params.page == undefined) {
            res.redirect("/admin/order/list");
          } else {
            next();
          }
        } else {
          console.log(4564);
          if (req.params.page == "login") {
            res.render('login', {
              pageTitle: '登录页'
            });
          } else {
            res.redirect("/admin/login");
          }
        }

        console.log('Accessing the secret section ...');
        //next(); // pass control to the next handler
      });

      // User
      app.post('/login', Manager.login)
      app.get('/signout', Manager.signout)

      //order
      app.get('/admin/order/list', Order.list);
      app.post('/admin/order/add', Order.add)
      app.post('/admin/order/update/id/:id?', Order.update);
      app.get('/admin/order/details/id/:id?', Order.details);

      //product
      app.get('/admin/product/list', Product.list);
      app.post('/admin/product/add', Product.add)
      app.post('/admin/product/update/id/:id?', Product.update);
      app.post('/admin/product/del/id/:id', Product.del);
      app.get('/admin/product/details/id/:id?', Product.details);

      //cart
      app.get('/admin/cart/list', Cart.list);
      app.post('/admin/cart/add', Cart.add);
      app.post('/admin/cart/update/id/:id?', Cart.update);
      app.get('/admin/cart/details/id/:id?', Cart.details);
      
      //users
      app.get('/admin/users/list', User.list);
      app.post('/admin/users/add', User.add)
      app.post('/admin/users/update/id/:id?', User.update);
      app.get('/admin/users/details/id/:id?', User.details);

      //-------前台
      //前台列表页
      app.get('/front/list', function(req, res) {
        get_list(function(docs) {
          res.send({
            data: docs
          });
        });
      });

      //前台详情页
      app.get('/front/details/id/:id', function(req, res) {
        var details_id = req.params.id;
        console.log(details_id);
        get_details(details_id, function(docs) {
          res.send(docs);
        });

      });


}


