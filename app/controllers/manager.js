var Manager = require('../models/manager.js');

// login
exports.login = function(req, res) {
      req.on('data',function(data){
        var obj = JSON.parse(data);
        Manager.find( obj, req, res, function(err,docs){
                if( obj.password == docs[0].password ){ 
                  req.session.lastSign = docs[0]._id;
                  res.send({"status":"s","msg":"登录成功"});
                }else{  
                  res.send({"status":"e","msg":"密码错误"});
                }
        });
      })
}

// signout
exports.signout = function(req, res) {
    req.session.destroy(function(err) {
      // cannot access session here 
      res.redirect("/admin/login");
    });
}