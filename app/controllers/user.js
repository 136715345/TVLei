var User = require('../models/user.js');
var Cart = require('../models/cart.js');
var Product = require('../models/product.js');
var Order = require('../models/order.js');

exports.list = function(req, res) {
  User.list(function(docs){
          if( docs.length <= 0 ){
            res.render('user_list',{pageTitle:'用户列表'});
          }else{
            res.render('user_list',{pageTitle:'用户列表',data:docs});
          }
      });
}

exports.add = function(req, res) {
  var obj = JSON.parse(body.toString());
  User.add(obj,function(res,req){
    res.send('保存数据成功');
  });
}

exports.update = function(req,res){
        req.on('data',function(data){
          var obj = JSON.parse(data.toString());
          obj.date = new Date().getFullYear()+"-"+parseInt( new Date().getMonth()+1 )+"-"+new Date().getDate();
          // if( !obj.editer || obj.editer==undefined || obj.editer==null  )
          //   obj.editer = 'zz';
          // if( !req.params.id || req.params.id == undefined){
          //   //新建
          //   update_pro( obj, function( docs ){
          //     res.send({"status":"s","msg":"创建成功","_id":docs._id});
          //   } );
          // }else{
            //修改
            var pro_id=req.params.id;
            User.update( obj, function( docs ){
              res.send({"status":"s","msg":"修改成功","_id":docs._id});
            }, pro_id );
          //}
        })
      }

function recursionGetPro( i, datas, callback ){

  Product.details( datas.cartPro[i].pro_id, function( docs ){
      datas.cartPro[i].pro_name = docs[0].name;
      datas.cartPro[i].pro_type = docs[0].type;
      datas.cartPro[i].pro_pics = docs[0].pics[0];
      datas.cartPro[i].pro_price = docs[0].price;
      if( i >= datas.cartPro.length - 1 ){
        callback( datas );
      }else{
        i++;
        recursionGetPro( i,datas,callback );
      }
    });
    
}

function getOrderPrice( m, datas, next ){

  Order.details( datas.orders[m], function( docs ){
      datas.total = datas.total + docs[0].totalamount*1;

      if( m >= datas.orders.length - 1 ){
        next( datas );
      }else{
        m++;
        getOrderPrice( m,datas,next );
      }
    });
    
}

exports.details = function(req, res) {
  var user_id = req.params.id
  var datas = {};
  User.details( user_id, function( docs ){
    datas = docs;
    datas.total = 0;
    var m = 0;

    getOrderPrice( m, datas, function(datas){

        Cart.details(datas.user_id, function( docs ){
            datas.cartPro = docs.products;
            var i = 0;
            if( datas.cartPro.length > i ){
                recursionGetPro( i, datas, function( mergeddatas ){
                   res.render('user_details',{pageTitle:'用户详情',data:mergeddatas});
                } );
            }
        });

    });
    
  });

}