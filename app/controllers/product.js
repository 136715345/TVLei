var Product = require('../models/product.js');

// list page
exports.list = function(req,res){
        Product.list( function( docs ){
            // if( req.path.split(/\//)[0] != 'admin' ){
            //     res.send({data:docs});
            // }else{
              console.log(docs);
                res.render('pro_list',{pageTitle:'商品列表页',data:docs});
            //}
        });
      }

// admin new page
exports.add = function(req,res){
        /*var maxdata = 10*1024*1024; 
        req.set({
          'Content-Type': 'text/plain',
          'Content-Length': 10*1024*1024,
          'ETag': '12345'
        });*/
      console.log( req.headers['content-length'] );
        var body = "";
        req.on('data',function(data){
          body += data;
          //console.log( data );
          /*var obj = JSON.parse(data.toString());
          obj.date = new Date().getFullYear()+"-"+parseInt( new Date().getMonth()+1 )+"-"+new Date().getDate();
          if( !obj.editer || obj.editer==undefined || obj.editer==null  ){
            obj.editer = 'zz';
          }
          var img=null;
          for ( img in obj.images ) {
            //接收前台POST过来的base64
              var imgData = obj.images[img].src;
              //过滤data:URL
              var base64Data = imgData.replace(/^data:image\/\w+;base64,/, "");
              var dataBuffer = new Buffer(base64Data, 'base64');
              (function(img) {
                fs.writeFile(__dirname+"/public/imgs/"+img+".png", dataBuffer, function(err) {
                    if(err){
                      res.send(err);
                    }else{
                      obj.images[img].src = __dirname+"/public/imgs/"+img+".png";
                      db.open(function(err,db){
                    db.collection('index',function(err,collection){
                      collection.update({},obj,{upsert:true},function(err,docs){
                        if( img == 'img2' ){
                          res.send('保存数据成功');
                        }
                        
                        db.close();
                      });
                    })
                   })
                    }
                });
            })(img);
          };*/
          
          console.log(222);
        }).on('end', function () { 
          var obj = JSON.parse(body.toString());
          obj.date = new Date().getFullYear()+"-"+parseInt( new Date().getMonth()+1 )+"-"+new Date().getDate();
          // if( !obj.editer || obj.editer==undefined || obj.editer==null  ){
          //   obj.editer = 'zz';
          // }
          var img=null;
          for ( img in obj.images ) {
            //接收前台POST过来的base64
              var imgData = obj.images[img].src;
              //过滤data:URL
              var base64Data = imgData.replace(/^data:image\/\w+;base64,/, "");
              var dataBuffer = new Buffer(base64Data, 'base64');
              (function(img) {
                fs.writeFile(__dirname+"/app/images/"+img+".png", dataBuffer, function(err) {
                    if(err){
                      res.send(err);
                    }else{
                      obj.images[img].src = __dirname+"/app/images/"+img+".png";

                      Product.add(obj,function(res,req){
                        res.send('保存数据成功');
                      });

                      
                    }
                });
              })(img);
          };

          console.log(8888);
        }); 
      }

// list page
exports.del = function(req,res){
        if( !req.params.id || req.params.id == undefined){
          res.send({"status":"e","msg":"此商品不存在"});
        }else{
          var pro_id=req.params.id;

          Product.del(pro_id,function(res,req){
            res.send({"status":"s","msg":"删除成功"});
          });

        }
      }

function update_pro( data, callback, pro_id ){
  if( !pro_id ){
    db.open(function(err,db){
      db.collection('products',function(err,collection){
        collection.insert(data,function(err,docs){
          console.log(docs);
          callback(docs.ops[0]);
          db.close();
        });
      })
    })
  }else{
    db.open(function(err,db){
      db.collection('products',function(err,collection){
        collection.findAndModify({_id:ObjectID(pro_id)},{_id:1},data,function(err,docs){
          console.log(docs);
          callback(docs.value);
          db.close();
        });
      })
    })
  }
}

// admin update page
exports.update = function(req,res){
        req.on('data',function(data){
          var obj = JSON.parse(data.toString());
          obj.date = new Date().getFullYear()+"-"+parseInt( new Date().getMonth()+1 )+"-"+new Date().getDate();
          // if( !obj.editer || obj.editer==undefined || obj.editer==null  )
          //   obj.editer = 'zz';
          // if( !req.params.id || req.params.id == undefined){
          //   //新建
          //   update_pro( obj, function( docs ){
          //     res.send({"status":"s","msg":"创建成功","_id":docs._id});
          //   } );
          // }else{
            //修改
            var pro_id=req.params.id;
            Product.update( obj, function( docs ){
              res.send({"status":"s","msg":"修改成功","_id":docs._id});
            }, pro_id );
          //}
        })
      }

// detail page
exports.details = function(req, res) {
  var pro_id = req.params.id

  Product.details( pro_id, function( docs ){
    console.log(docs);
    res.render('pro_details',{pageTitle:'商品详情页',data:docs[0]});
  });
  
}


