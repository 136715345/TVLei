var Order = require('../models/order.js')
var User = require('../models/user.js');

//后台订单列表查询
exports.list = function(req, res) {
      Order.list(function(docs){
          // if( docs.length <= 0 ){
          //   res.render('order_list',{pageTitle:'订单列表'});
          // }else{
            console.log(docs);
            res.render('order_list',{pageTitle:'订单列表',data:docs});
          //}
      });
}

// 有人下订单
exports.add = function(req, res) {
  var obj = JSON.parse(body.toString());
  Order.add(obj,function(res,req){
    res.send('保存数据成功');
  });
}

// 修改订单（状态）
exports.update = function(req, res) {
  var obj = JSON.parse(body.toString());
  Order.update(obj,function(res,req){
    res.send('保存数据成功');
  });
} 

// 查询订单详情
exports.details = function(req,res,next){
        if( !req.params.id || req.params.id == undefined){
          res.render('orderDetails',{pageTitle:'订单详情页',data:{}});
        }else{
          var details_id=req.params.id;
          Order.details( details_id,function( docs ){
            if( docs.length > 0 ){
              var user_id = docs[0].user_id;
              User.details(user_id,function(userDate){
                console.log(userDate);
                docs[0].userName = userDate.name;
                docs[0].userTel = userDate.tel;
                docs[0].userAdd = userDate.add;
                console.log(docs[0]);
                res.render('order_details',{pageTitle:'订单详情页',data:docs[0]});
              });
              
            }else{
              res.render('order_details',{pageTitle:'订单详情页',data:{}});
            }

          });

        }
      }