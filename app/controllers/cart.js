var Cart = require('../models/cart.js')

exports.list = function(req, res) {
  Cart.list(function(docs){
          if( docs.length <= 0 ){
            res.render('cart_list',{pageTitle:'购物车列表'});
          }else{
            res.render('cart_list',{pageTitle:'购物车列表',data:docs[0]});
          }
      });
}

exports.add = function(req, res) {
  var obj = JSON.parse(body.toString());
  Cart.add(obj,function(res,req){
    res.send('保存数据成功');
  });
}

// list page
exports.del = function(req,res){
        if( !req.params.id || req.params.id == undefined){
          res.send({"status":"e","msg":"此商品不存在"});
        }else{
          var pro_id=req.params.id;

          Cart.del(pro_id,function(res,req){
            res.send({"status":"s","msg":"删除成功"});
          });

        }
      }

// admin update page
exports.update = function(req,res){
        req.on('data',function(data){
          var obj = JSON.parse(data.toString());
          obj.date = new Date().getFullYear()+"-"+parseInt( new Date().getMonth()+1 )+"-"+new Date().getDate();
          // if( !obj.editer || obj.editer==undefined || obj.editer==null  )
          //   obj.editer = 'zz';
          // if( !req.params.id || req.params.id == undefined){
          //   //新建
          //   update_pro( obj, function( docs ){
          //     res.send({"status":"s","msg":"创建成功","_id":docs._id});
          //   } );
          // }else{
            //修改
            var pro_id=req.params.id;
            Cart.update( obj, function( docs ){
              res.send({"status":"s","msg":"修改成功","_id":docs._id});
            }, pro_id );
          //}
        })
      }

// details page
exports.details = function(req, res) {
  var pro_id = req.params.id

  Cart.details( pro_id, function( docs ){
    res.send(docs);
  });
  
}