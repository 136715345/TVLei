var db = require('../models/public.js').db;
var ObjectID = require('../models/public.js').ObjectID;

exports.list = function(callback) {
      db.open(function(err,db){
        db.collection('carts',function(err,collection){
          if(err) throw err;
          else{
            collection.find({}).toArray(function(err,docs){
              console.log(docs);
              callback(docs);
              db.close();
            });
          }
        })
      })
}

exports.add = function(obj,callback){
    db.open(function(err,db){
          db.collection('carts',function(err,collection){
              collection.insert(obj,{upsert:true},function(err,docs){
                if(err) throw err; 
                //if( img == 'img2' ){
                  callback();
                  
                //}
                
                db.close();
              });
          })
      })
}

 exports.del = function(pro_id,callback){
          db.open(function(err,db){
            db.collection('carts',function(err,collection){
              collection.remove({_id:ObjectID(pro_id)},{single:false},function(err,docs){
                if(err) throw err;
                else{
                  res.send({"status":"s","msg":"删除成功"});
                  db.close();
                }
              });
            })
          })
 }

 exports.update = function(data, callback, pro_id ){
  db.open(function(err,db){
      db.collection('carts',function(err,collection){
        collection.findAndModify({_id:ObjectID(pro_id)},{_id:1},data,function(err,docs){
          if(err) throw err;
          else{ 
            callback(docs.value);
            db.close();
          }
        });
      })
    })
 }

exports.details = function(user_id, callback) {

  db.open(function(err,db){
            db.collection('carts',function(err,collection){
              if(err) throw err;
              else{
                collection.find({"user_id":user_id}).toArray(function(err,docs){
                  db.close();
                  callback(docs[0]);
                });
              }
            })
          })

}
