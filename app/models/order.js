var db = require('../models/public.js').db;

//后台订单列表查询
exports.list = function(callback) {
      db.open(function(err,db){
        db.collection('orders',function(err,collection){
          if(err) throw err;
          else{
            collection.find({}).toArray(function(err,docs){
              console.log(docs);
              db.close();
              callback(docs);
            });
          }
        })
      })
}

// 有人下订单
exports.add = function(obj,callback){
    db.open(function(err,db){
          db.collection('orders',function(err,collection){
              collection.insert(obj,{upsert:true},function(err,docs){
                if(err) throw err; 
                //if( img == 'img2' ){
                  callback();
                  
                //}
                
                db.close();
              });
          })
      })
}

// 修改订单（状态）
exports.update = function(data, callback, pro_id ){
  db.open(function(err,db){
      db.collection('orders',function(err,collection){
        collection.findAndModify({_id:ObjectID(pro_id)},{_id:1},data,function(err,docs){
          if(err) throw err;
          else{ 
            callback(docs.value);
            db.close();
          }
        });
      })
    })
 }


// 查询订单详情
exports.details =  function ( details_id,callback ){
        db.open(function(err,db){
            db.collection('orders',function(err,collection){
              if(err) throw err;
              else{
                collection.find({"order_num":details_id}).toArray(function(err,docs){
                  db.close();
                  callback(docs);
                });
              }
            })
          })
      };