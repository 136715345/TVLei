var mongo = require('mongodb');
var ObjectID = require('mongodb').ObjectID;
var host = '127.0.0.1';
var port = '27017';
var server = new mongo.Server(host,port,{auto_reconnect:true});
exports.ObjectID = ObjectID;
exports.db = new mongo.Db('userDate',server,{safe:true});
